from PyQt5 import QtWidgets
from Uni_table_model import UnifiedTableModel


class TableDataArea(QtWidgets.QWidget):

    def __init__(self, parent, data_name, titles, update_data_function):
        super().__init__(parent)

        self.layout = QtWidgets.QVBoxLayout(self)

        self.add_new_instance_button = QtWidgets.QPushButton(f"Add new {data_name}")

        self.data_table_model = UnifiedTableModel(titles, update_data_function)
        self.data_table = QtWidgets.QTableView(self)
        self.data_table.setModel(self.data_table_model)
        self.data_table.horizontalHeader() \
            .setSectionResizeMode(QtWidgets.QHeaderView.ResizeMode.ResizeToContents)
        self.data_table.setSelectionBehavior(QtWidgets.QListWidget.SelectionBehavior.SelectRows)
        self.data_table.setSelectionMode(QtWidgets.QListWidget.SelectionMode.SingleSelection)

        self.layout.addWidget(self.data_table)
        self.layout.addWidget(self.add_new_instance_button)

    def update(self) -> None:
        self.data_table.update()
        super().update()


class SearchTableArea(TableDataArea):
    def __init__(self, parent, data_name, titles, update_data_function, search_fields: [[str, str]]):
        super().__init__(parent, data_name, titles, update_data_function)

        self.search_lines = {}
        search_layout = QtWidgets.QVBoxLayout()

        search_lines = QtWidgets.QFormLayout()
        for key, lable in search_fields:
            self.search_lines[key] = QtWidgets.QLineEdit()
            search_lines.addRow(lable, self.search_lines[key])

        search_layout.addWidget(QtWidgets.QLabel("Searching"))
        search_layout.addLayout(search_lines)

        self.layout.insertLayout(0, search_layout)

    def clear_searching(self):
        for line in self.search_lines.values():
            line.clear()
