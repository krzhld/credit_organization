from PyQt5.QtWidgets import QMessageBox, QMainWindow, QApplication
from Main_window_interface_class import MainWindowUI
from Main_window_actions import MainWindowActions

from DB_class import MyDbRequests


class DbGui(QMainWindow):

    def __init__(self):
        super(DbGui, self).__init__()
        MyDbRequests()

        self.ui = MainWindowUI(self)
        self.action = MainWindowActions(self.ui)

    # def closeEvent(self, event) -> None:
    #     reply = QMessageBox.StandardButton.Yes
    #     counter = 0
    #     while reply == QMessageBox.StandardButton.Yes:
    #         reply = QMessageBox.question(self, "Message",
    #                                      "Are you {}sure to quit?".format("".join(["really "] * counter)),
    #                                      QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No,
    #                                      QMessageBox.StandardButton.Yes)
    #         counter += 1
    #         if counter == 20:
    #             for i in range(10):
    #                 QMessageBox.information(self, "Тёма", "Ну лааааааа{}дно...".format("".join(["a"] * (7 + i))))
    #             break
    #
    #     if reply == QMessageBox.StandardButton.Yes:
    #         event.accept()
    #     else:
    #         event.ignore()


if __name__ == "__main__":
    app = QApplication([])
    application = DbGui()
    application.showMaximized()
    exit(app.exec())
