from PyQt5 import QtWidgets

from Uni_table_model import UnifiedTableModel


class WarningMessageGenerationWidget(QtWidgets.QWidget):
    def __init__(self, parent, titles, update_function, message_pattern):
        super().__init__(parent)

        layout = QtWidgets.QVBoxLayout(self)

        self.pattern_text = QtWidgets.QTextEdit()
        self.pattern_text.setText(message_pattern)

        self.messages_text = QtWidgets.QTextEdit()

        self.generate_button = QtWidgets.QPushButton("Generate messages")

        self.report_table_model = UnifiedTableModel(titles, update_function)
        self.report_table = QtWidgets.QTableView(self)
        self.report_table.setModel(self.report_table_model)
        self.report_table.horizontalHeader() \
            .setSectionResizeMode(QtWidgets.QHeaderView.ResizeMode.ResizeToContents)
        self.report_table.setSelectionBehavior(QtWidgets.QListWidget.SelectionBehavior.SelectRows)
        self.report_table.setSelectionMode(QtWidgets.QListWidget.SelectionMode.SingleSelection)

        layout.addWidget(self.report_table)
        layout.addWidget(self.pattern_text)
        layout.addWidget(self.generate_button)
        layout.addWidget(self.messages_text)

    def update(self) -> None:
        self.report_table_model.update_info()
        self.report_table.update()
        super().update()
