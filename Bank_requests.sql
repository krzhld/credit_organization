DROP TABLE clients CASCADE

DROP TABLE credits CASCADE

DROP TABLE protocols CASCADE

CREATE TABLE clients(
	client_id SERIAL PRIMARY KEY,
	full_name TEXT NOT NULL,
	type_credit_story VARCHAR NOT NULL DEFAULT 'clear',
	CONSTRAINT credit_story_check CHECK (type_credit_story IN ('good', 'bad', 'clear'))
)

CREATE TABLE credits(
	credit_id SERIAL PRIMARY KEY,
	client_id INT NOT NULL,
	bank_name TEXT NOT NULL,
	credit_amount NUMERIC NOT NULL DEFAULT 0,
	annual_percentage INT NOT NULL DEFAULT 7,
	issue_date DATE NOT NULL,
	month_to_pay INT NOT NULL,
	early_repayment NUMERIC NOT NULL DEFAULT 0,
	status TEXT NOT NULL DEFAULT 'IN PROCESS',
	CONSTRAINT status_check CHECK (status IN ('IN PROCESS', 'PAID')),
	FOREIGN KEY (client_id) REFERENCES clients(client_id) ON DELETE RESTRICT
)


CREATE TABLE protocols(
	protocol_id SERIAL PRIMARY KEY,
	credit_id INT NOT NULL,
	plan_payment_date DATE NOT NULL,
	plan_payment_amount NUMERIC NOT NULL,
	real_payment_amount NUMERIC NOT NULL DEFAULT 0,
	penalty NUMERIC NOT NULL DEFAULT 0,
	FOREIGN KEY (credit_id) REFERENCES credits(credit_id) ON DELETE RESTRICT,
	UNIQUE (credit_id, plan_payment_date)
)

DROP TABLE global_date CASCADE

CREATE TABLE global_date(
	date DATE NOT NULL,
	check_new_month DATE NOT NULL
)


DROP FUNCTION month_diff(date, date)

CREATE FUNCTION month_diff(date, date)
RETURNS int AS $$
BEGIN
		RETURN abs((EXTRACT(year FROM $1::date) - EXTRACT(year FROM $2::date))* 12 +
	(EXTRACT(months FROM $1::date) - EXTRACT(months FROM $2::date)));
END;
$$ LANGUAGE plpgsql;


INSERT INTO global_date VALUES (current_date - 3000, current_date)

UPDATE global_date SET
check_new_month = date_trunc('month', date)

SELECT * FROM GLOBAL_date


/*month*/
UPDATE global_date SET
date = date + INTERVAL'1month'

/*day*/
UPDATE global_date SET
date = date + 1


/*Addig values requests*/
INSERT INTO client(full_name) VALUES ($name)

WITH new_credit AS (
	INSERT INTO credits(client_id, bank_name, credit_amount, issue_date, month_to_pay)
	VALUES
	(	$client_id,
		$bank_name,
		$credit_amount,
		(SELECT date FROM global_date),
		$months_to_pay
	)
	RETURNING *
)


INSERT INTO protocols (credit_id, plan_payment_date, plan_payment_amount)
SELECT credit_id, MONTH, plan_payment_amount FROM protocols_v
WHERE month >= (SELECT date FROM global_date)
AND MONTH < (SELECT date + INTERVAL'1month' FROM global_date)
AND credit_id = $id


/*------------*/



SELECT * FROM credits c
LEFT OUTER JOIN protocols p ON p.credit_id = c.credit_id



/*random insertion*/

DROP VIEW random_names 

CREATE VIEW random_names AS 
WITH names_arr AS (
	SELECT ARRAY['Ivan', 'Bob', 'Hlop', 'Vlob', 'Vasa', 'Vaza', 'Vasya', 'Chel', 'Valdemar', 'Igorek', 'Zhenek', 'Playstation', 'Shish', 'Lyonya']
), second_names_arr AS (
	SELECT ARRAY['Lapital', 'Pascal', 'Nakal', 'Kuznei', 'Kuznec', 'Molodec', 'Dyadya', 'Pilulkin', 'Pupkin', 'Gubkin', 'Chelkin', 'Shishkin', 'Yakubovich']
), last_names_arr AS (
	SELECT ARRAY['Ivanich', 'Ivanovich', 'Petrovich', 'Genadievich', 'Olegovich', 'Ugarich', 'Papich', 'Chelich', 'Yakubovich']
), name as(
	SELECT DISTINCT ON (i) * FROM
	(SELECT i FROM generate_series(0, 99 ) AS gs(i) ORDER BY random()) AS i,
	UNNEST((SELECT * FROM names_arr)) AS value
), second_name as(
	SELECT DISTINCT ON (i)  * FROM
	(SELECT i FROM generate_series(0, 99) AS gs(i) ORDER BY random()) AS i,
	UNNEST((SELECT * FROM second_names_arr)) AS value
), last_name as(
	SELECT DISTINCT ON (i) * FROM
	(SELECT i FROM generate_series(0, 99) AS gs(i) ORDER BY random()) AS i,
	UNNEST((SELECT * FROM last_names_arr)) AS value
) SELECT n.value || ' ' || s.value || ' ' || l.value AS full_names FROM name AS n
JOIN second_name AS s ON n.i = s.i
JOIN last_name AS l ON n.i = s.i AND s.i = l.i AND l.i = n.i

INSERT INTO clients(full_name)
(SELECT * FROM random_names LIMIT 100)


DROP VIEW random_credit;

CREATE VIEW random_credit AS 
WITH random_clients as(
--	SELECT client_id AS i, * FROM clients
	SELECT DISTINCT ON (i) i, client_id FROM
		(SELECT i FROM generate_series(0, 99) AS gs(i) ORDER BY (random() + 0.1)) AS i,
		(SELECT * FROM clients ORDER BY (random() + 0.1)) AS cl
), bank_names_arr AS (
	SELECT ARRAY['Lyonya', 'Balon', 'Furgalon', 'Vavilon', 'Tinkolon', 'Sberoblom']
), random_bank_names AS (
	SELECT DISTINCT ON (i) i, UNNEST AS value FROM
	(SELECT i FROM generate_series(0, 99 ) AS gs(i) ORDER BY random()) AS i,
	(SELECT * FROM UNNEST((SELECT * FROM bank_names_arr)) ORDER BY random() + 0.1) AS value
)
SELECT
	DISTINCT ON (client_id)
	client_id ,
	value AS bank_name,
	CEIL(random() * 100000) AS credit_amount,
	CEIL(random() * 10) + 1 AS percentage,
	date_trunc('month', (SELECT date FROM global_date))::date AS issue_date,
	ceil((random()+0.1) * 100) AS months
FROM random_clients c
JOIN random_bank_names b ON c.i = b.i
ORDER BY client_id


WITH new_credit AS (
	INSERT INTO credits (client_id, bank_name, credit_amount,annual_percentage,  issue_date, month_to_pay)
	SELECT * FROM random_credit
	RETURNING *
)
SELECT * FROM new_credit

--INSERT INTO protocols (credit_id, plan_payment_date, plan_payment_amount)
--SELECT credit_id, MONTH, plan_payment_amount FROM protocols_v
--WHERE date_trunc('month', MONTH) = date_trunc('month',(SELECT date FROM global_date) + INTERVAL'1month')

--SELECT * FROM credits c 






DROP VIEW protocols_v CASCADE

CREATE VIEW protocols_v AS
WITH month_percetges AS(
	SELECT credit_id, annual_percentage::float / 100 / 12 AS mp, month_to_pay AS months FROM credits
), month_coef AS (
	SELECT credit_id, ((mp * (1 + mp)^months) / ((1 + mp)^months -1)) AS k FROM month_percetges
), peny_addiction AS (
	SELECT * FROM protocols p
	WHERE plan_payment_date - INTERVAL'1month' >= (SELECT date + INTERVAL'1month' FROM global_date)
	AND plan_payment_date - INTERVAL'2month' < (SELECT date + INTERVAL'1month' FROM global_date)
), annual_protocol as(
	SELECT
		mc.credit_id,
		cl.client_id,
		full_name,
		bank_name,
		issue_date,
		month_to_pay,
		round((k*(credit_amount - early_repayment + COALESCE(penalty, 0)))::NUMERIC ,2) AS plan_payment_amount
	FROM clients cl 
	LEFT OUTER JOIN credits cr ON cl.client_id = cr.client_id
	LEFT OUTER JOIN month_coef mc ON mc.credit_id = cr.credit_id
	LEFT OUTER JOIN peny_addiction pa ON pa.credit_id = cr.credit_id
	WHERE status LIKE 'IN PROCESS'
), months_gen AS (
	SELECT client_id, date_trunc('month', issue_date + INTERVAL'1month' * generate_series(1, month_to_pay))::date AS MONTH FROM annual_protocol
) SELECT
	m.client_id,
	a.credit_id,
	FULL_name,
	MONTH,
	bank_name,
	plan_payment_amount
FROM months_gen m
JOIN annual_protocol a ON a.client_id = m.client_id


SELECT * FROM protocols_v

INSERT INTO protocols (credit_id, plan_payment_date, plan_payment_amount)
SELECT credit_id, MONTH, plan_payment_amount FROM protocols_v
WHERE date_trunc('month', MONTH) = date_trunc('month',(SELECT date FROM global_date) + INTERVAL'1month')


SELECT * FROM protocols_v
WHERE date_trunc('month', MONTH) = date_trunc('month',(SELECT date FROM global_date) + INTERVAL'1month')
ORDER BY client_id, credit_id, month



UPDATE credits
SET annual_percentage = 3
WHERE annual_percentage = 0










/*Date update circle*/
UPDATE protocols SET 
	plan_payment_amount = plan_payment_amount + penalty * 0.01
WHERE (SELECT date FROM global_date) >= plan_payment_date - INTERVAL'1month'
	AND (SELECT date FROM global_date) < plan_payment_date

UPDATE global_date SET
	date = date + 1
WHERE TRUE


/*Month update*/
WITH bad_pay_credits AS (
	SELECT * FROM credits c 
	WHERE (date_trunc('month', issue_date) + (1 + month_to_pay)*INTERVAL'1month')
	<= (SELECT date FROM global_date gd)
	AND status NOT LIKE 'PAID'
)
UPDATE clients SET
	type_credit_story = 'bad'
WHERE client_id IN (SELECT client_id FROM bad_pay_credits)

--UPDATE credits SET
--	month_to_pay = month_to_pay + 1
--WHERE credit_id IN (SELECT credit_id FROM bad_pay_credits)


INSERT INTO protocols (credit_id, plan_payment_date, plan_payment_amount)
SELECT credit_id, MONTH, plan_payment_amount FROM protocols_v
WHERE (SELECT date FROM global_date) >= MONTH - INTERVAL'1month'
	AND (SELECT date FROM global_date) < month
	AND (credit_id, MONTH) NOT IN (SELECT credit_id, plan_payment_date FROM protocols)
	AND date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
	= pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date))

SELECT * FROM protocols p 

UPDATE protocols SET
	penalty = 0

WITH user_payment_update as(
	SELECT
		*
	FROM protocols p
	WHERE (SELECT date FROM global_date) >= p.plan_payment_date
	AND (SELECT (date - INTERVAL '1month')::date FROM global_date) < p.plan_payment_date
	AND date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
	= pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date))
)
UPDATE protocols ps SET
	penalty = COALESCE((SELECT 
	DISTINCT ON (u.protocol_id)
		u.penalty +
		COALESCE(
		CASE
			WHEN (u.real_payment_amount < u.plan_payment_amount) THEN u.plan_payment_amount - u.real_payment_amount
			ELSE 0
		END,
		0)
		FROM user_payment_update u
		WHERE date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
			= pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date))
		AND u.credit_id = ps.credit_id
		AND (u.plan_payment_date + INTERVAL '1 month')::date = ps.plan_payment_date), 0)
WHERE 
	(ps.credit_id, (ps.plan_payment_date - INTERVAL '1 month')::date)
		IN (SELECT credit_id, plan_payment_date FROM user_payment_update)
	AND 
	date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
	= pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date))

	
UPDATE global_date SET
check_new_month = date_trunc('month', date) 
WHERE 
	date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
	= pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date))






/*Payment of a debt*/
--WITH user_payment_check as(
--	SELECT protocol_id, cl.client_id, full_name FROM protocols p
--	JOIN credits cr ON cr.credit_id = p.credit_id
--	JOIN clients cl ON cl.client_id = cr.client_id
--	WHERE (SELECT date FROM global_date) >= p.plan_payment_date - INTERVAL'1month'
--	AND (SELECT date FROM global_date) < p.plan_payment_date
--)

/*Payment of a debt*/
UPDATE protocols SET 
	penalty = penalty - $value
WHERE credit_id = $id
AND (SELECT date FROM global_date) >= plan_payment_date - INTERVAL'1month'
AND (SELECT date FROM global_date) < plan_payment_date
RETURNING *


WITH real_payment_amount_update AS(
	UPDATE protocols SET
		real_payment_amount = real_payment_amount - penalty,
		penalty = 0
	WHERE penalty < 0
	RETURNING *
)
UPDATE credits c SET
	early_repayment = early_repayment + 
	CASE
		WHEN (COALESCE ((SELECT real_payment_amount - plan_payment_amount 
	FROM real_payment_amount_update pu
	WHERE pu.credit_id = c.credit_id), 0) >= 0))
		THEN COALESCE ((SELECT real_payment_amount - plan_payment_amount 
	FROM real_payment_amount_update pu
	WHERE pu.credit_id = c.credit_id), 0)
	ELSE 0
WHERE credit_id IN (SELECT credit_id FROM real_payment_amount_update u)
RETURNING *

	
WITH client AS (
	(SELECT DISTINCT ON (credit_id) client_id FROM protocols_v pv 
	WHERE plan_payment_amount <= 0)
)
UPDATE clients c SET
	type_credit_story = 'good'
WHERE client_id = (SELECT * FROM client)
	AND (SELECT type_credit_story FROM clients WHERE client_id = (SELECT * FROM client)) NOT LIKE 'bad'
	
--finish credit
WITH last_penalty AS (
	SELECT credit_id, penalty
	FROM protocols p2 
	WHERE (SELECT date FROM global_date) >= plan_payment_date - INTERVAL '1 month'
	AND (SELECT date FROM global_date) < plan_payment_date
), payment_statistic AS(
	SELECT
		p.credit_id,
		sum(plan_payment_amount) AS sum_plan_payment_amount,
		sum(real_payment_amount) AS sum_real_payment_amount,
		COALESCE(sum(lp.penalty), 0) AS last_penalty,
		sum(real_payment_amount) - sum(plan_payment_amount) - COALESCE(sum(lp.penalty), 0) AS payment_result
	FROM full_time_protocols p 
	LEFT OUTER JOIN last_penalty lp ON lp.credit_id = p.credit_id
	GROUP BY p.credit_id
), should_to_close_credits_id AS(
	SELECT c.credit_id 
	FROM payment_statistic ps
	JOIN credits c ON c.credit_id = ps.credit_id AND status NOT LIKE 'PAID'
	WHERE payment_result >= 0
)
UPDATE credits SET
	status = 'PAID',
	month_to_pay = month_diff(
                    issue_date,
                    (SELECT month FROM protocols_v pv 
                    WHERE credit_id = pv.credit_id
                    ORDER BY MONTH
                    LIMIT 1))
WHERE credit_id IN (SELECT credit_id FROM should_to_close_credits_id)

--SELECT COALESCE(sum(real_payment_amount), 0) FROM protocols pv
--
--SELECT credit_amount*(1 + annual_percentage::float/100), * FROM credits c 
--	
--(SELECT DISTINCT ON (credit_id) * FROM credits c 
--        WHERE credit_id IN (SELECT DISTINCT ON (credit_id) credit_id FROM protocols_v pv WHERE plan_payment_amount <= 0)
--        ORDER BY credit_id)
--	
--SELECT * FROM protocols_v pv
--ORDER BY month
--    
--SELECT * FROM protocols p 


SELECT 
	full_name,
	bank_name,
	credit_amount,
	annual_percentage,
	issue_date,
	(issue_date + INTERVAL '1month' * month_to_pay)::date AS end_date
	FROM credits cr
JOIN clients cl ON cl.client_id = cr.client_id

SELECT * FROM clients 
WHERE 
full_name LIKE '%Zh%'
AND full_name LIKE '%Z%'



SELECT * FROM credits 
ORDER BY early_repayment 

DROP VIEW full_time_protocols

--CREATE VIEW full_time_protocols AS
--SELECT 
--	credit_id,
--	plan_payment_date,
--	plan_payment_amount,
--	real_payment_amount,
--	penalty
--FROM protocols
--UNION ALL
--SELECT credit_id, MONTH, plan_payment_amount, 0, 0 FROM protocols_v pv 
--WHERE (SELECT date FROM global_date) >= month
--	AND (SELECT date FROM global_date) < MONTH + INTERVAL'1month'
--ORDER BY credit_id, plan_payment_date

CREATE VIEW full_time_protocols AS
SELECT 
	credit_id,
	plan_payment_date,
	plan_payment_amount,
	real_payment_amount,
	penalty
FROM protocols
UNION ALL
SELECT credit_id, MONTH, plan_payment_amount, 0, 0
FROM protocols_v pv 
WHERE (SELECT date + INTERVAL '1 month' FROM global_date) < MONTH
ORDER BY credit_id, plan_payment_date



/*Get all protocols history*/
SELECT
	c.client_id,
	full_name,
	bank_name,
	c.credit_id,
	(plan_payment_date - INTERVAL'1month')::date::TEXT AS from,
	plan_payment_date::TEXT AS to,
	plan_payment_amount::TEXT || '$' AS plan_payment,
	real_payment_amount::TEXT || '$' AS real_payment,
	penalty::TEXT || '$' AS penalty
FROM full_time_protocols fp
JOIN credits c ON fp.credit_id = c.credit_id
JOIN clients cl ON cl.client_id = c.client_id
ORDER BY full_name, bank_name, credit_id, plan_payment_date


SELECT DISTINCT ON (type_credit_story) type_credit_story FROM clients cl
JOIN credits cr ON cr.client_id = cl.client_id 
WHERE cl.client_id = $id



/*Get max available credit for client*/
WITH credit_story_type AS (
	SELECT DISTINCT ON (cl.client_id)
		cl.client_id,
		COALESCE(type_credit_story, 'clear') AS type_credit_story FROM clients cl
	LEFT OUTER JOIN credits cr ON cr.client_id = cl.client_id
	WHERE cl.client_id = 82
), client_max_credit AS (
	SELECT DISTINCT ON (type_credit_story)
		cs.client_id,
		type_credit_story,
		COALESCE(max(credit_amount),0) AS max
	FROM credits c
	RIGHT OUTER JOIN credit_story_type cs ON cs.client_id = c.client_id
	GROUP BY cs.type_credit_story, cs.client_id
)
SELECT
	CASE 
		WHEN (type_credit_story LIKE 'clear') THEN 1e6
		WHEN (type_credit_story LIKE 'good' AND max * 1.15 < 1e6) THEN 1e6
		WHEN (type_credit_story LIKE 'good' AND max * 1.15 >= 1e6) THEN max * 1.15
		ELSE max * 0.75
	END AS max_available_credit
FROM client_max_credit



/*Get current month payment*/
SELECT * FROM protocols p 
WHERE plan_payment_date > (SELECT date FROM global_date gd)
AND credit_id = $credit_id
ORDER BY plan_payment_date DESC 
LIMIT 1



/*Get all payment for finish credit*/
WITH paid AS (
	SELECT COALESCE(sum(real_payment_amount), 0) FROM protocols pv
	WHERE credit_id = $id
), penaltyes AS (
	SELECT penalty FROM protocols p 
	WHERE credit_id = $id
	ORDER BY plan_payment_date DESC 
	LIMIT 1
)
SELECT 
	CEIL(COALESCE((SELECT * FROM penaltyes)*1.01 + (credit_amount*(1 + annual_percentage/100)) - (SELECT * FROM paid), 
	(SELECT credit_amount FROM credits WHERE credit_id = $id)))
FROM protocols_v pv
	JOIN credits c ON c.credit_id = pv.credit_id 
WHERE month > (SELECT date FROM global_date gd)
AND c.credit_id = $id


/*Summons to court*/
SELECT 
	full_name,
	bank_name,
	c.credit_id,
	issue_date,
	issue_date + month_to_pay,
	credit_amount,
	annual_percentage,
	penalty
FROM protocols p
JOIN credits c ON 
	c.credit_id = p.credit_id
	AND
	p.penalty < c.credit_amount * 0.1
JOIN clients c2 ON c2.client_id = c.client_id
WHERE plan_payment_date < (SELECT date FROM global_date gd)
ORDER BY plan_payment_date

SELECT * FROM protocols p 

/*Agreement*/
SELECT 
	full_name,
	bank_name,
	credit_id,
	credit_amount::TEXT || '$' AS credit,
	annual_percentage::TEXT || '%' AS percentage,
	issue_date,
	issue_date + month_to_pay AS end_date,
	CASE
		WHEN (CEIL(month_to_pay / 12) > 0) THEN CEIL(month_to_pay / 12)::TEXT || ' years  ' || MOD(month_to_pay, 12)::TEXT || ' month'
		ELSE MOD(month_to_pay, 12)::TEXT || ' month'
	END AS term,
	type_credit_story
FROM credits c 
JOIN clients c2 ON c2.client_id = c.client_id
WHERE status LIKE 'PAID'


/*Credit penaltyes check*/
SELECT * FROM protocols p 
WHERE credit_id = 12

/*Banks info*/
WITH payment_info AS (
	SELECT
		bank_name,
		sum(real_payment_amount) AS get,
		sum(penalty) AS penny
	FROM protocols p 
		JOIN credits c ON c.credit_id = p.credit_id
	GROUP BY bank_name
) 
SELECT
	c.bank_name,
	sum(credit_amount) AS given,
	min(issue_date) AS from,
	max(issue_date + month_to_pay) AS TO,
	p.GET,
	penny
FROM credits c
	JOIN payment_info p ON p.bank_name = c.bank_name
GROUP BY c.bank_name, p.GET, penny
ORDER BY c.bank_name


SELECT * FROM protocols p 


/*dublicates protocols remove*/
WITH dublicate_mark AS (
	SELECT *,
	ROW_NUMBER() OVER (PARTITION BY credit_id, plan_payment_date order BY (credit_id, plan_payment_date)) AS mark
	FROM protocols p 
)
DELETE FROM protocols
WHERE protocol_id IN (SELECT protocol_id FROM dublicate_mark WHERE mark >=2)



CREATE TABLE protocols(
	protocol_id SERIAL PRIMARY KEY,
	credit_id INT NOT NULL,
	plan_payment_date DATE NOT NULL,
	plan_payment_amount NUMERIC NOT NULL,
	real_payment_amount NUMERIC NOT NULL DEFAULT 0,
	penalty NUMERIC NOT NULL DEFAULT 0,
	FOREIGN KEY (credit_id) REFERENCES credits(credit_id) ON DELETE RESTRICT,
	UNIQUE (credit_id, plan_payment_date)
)


SELECT * FROM credits c 

SELECT * FROM protocols p 

DELETE FROM credits 


-- money to finish
WITH last_penalty AS (
	SELECT credit_id, penalty
	FROM protocols p2 
	WHERE (SELECT date FROM global_date) >= plan_payment_date - INTERVAL '1 month'
	AND (SELECT date FROM global_date) < plan_payment_date
), payment_statistic AS(
	SELECT
		p.credit_id,
		sum(plan_payment_amount) AS sum_plan_payment_amount,
		sum(real_payment_amount) AS sum_real_payment_amount,
		COALESCE(sum(lp.penalty), 0) AS last_penalty,
		sum(real_payment_amount) - sum(plan_payment_amount) - COALESCE(sum(lp.penalty), 0) AS payment_result
	FROM full_time_protocols p 
	LEFT OUTER JOIN last_penalty lp ON lp.credit_id = p.credit_id
	GROUP BY p.credit_id
)SELECT -payment_result FROM payment_statistic
WHERE payment_result < 0 AND credit_id = $id
 
SELECT (NULL ISNULL)::bool


SELECT 
	CASE 
		WHEN (plan_payment_amount - real_payment_amount >= 0) THEN plan_payment_amount - real_payment_amount
		ELSE 0
	END + penalty AS need_to_pay
FROM protocols p2 
WHERE (SELECT date FROM global_date) >= plan_payment_date - INTERVAL '1 month'
AND (SELECT date FROM global_date) < plan_payment_date
AND credit_id = $id


SELECT * FROM credits c 

SELECT * FROM full_time_protocols 
