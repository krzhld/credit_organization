from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QInputDialog, QMessageBox

from Main_window_interface_class import MainWindowUI
from DB_class import MyDbRequests

from Adding_new_client_dialog import AddingNewClientDialog
from Adding_new_credit_dialog import AddingNewCreditDialog


class MainWindowActions:
    def __init__(self, ui: MainWindowUI):
        self.ui = ui

        self.__report_area_actions()
        self.__data_area_actions()
        self.date_timer = QTimer()
        self.date_timer.timeout.connect(self.__new_day_handler)
        self.date_timer.start(1000)
        self.date_flow_flag = True
        self.ui.time_flow_button.toggled.connect(self.__change_date_flow_status)

    def __change_date_flow_status(self):
        self.date_flow_flag = not self.date_flow_flag

    def __new_day_handler(self):
        if self.date_flow_flag:
            MyDbRequests.next_day()
            self.ui.date_line.setText(MyDbRequests.get_current_date())
            self.ui.data_visualization_widget.protocols_search_table.data_table_model.update_info()
            self.ui.data_visualization_widget.protocols_search_table.data_table.update()
            self.ui.report_widget.agreement_gen_widget.report_table_model.update_info()
            self.ui.report_widget.agreement_gen_widget.report_table.update()
            self.ui.report_widget.bank_report_gen_widget.report_table_model.update_info()
            self.ui.report_widget.bank_report_gen_widget.report_table.update()
            self.ui.report_widget.summons_to_court_gen_widget.report_table_model.update_info()
            self.ui.report_widget.summons_to_court_gen_widget.report_table.update()

    def __report_area_actions(self):
        self.ui.report_widget.report_combobox.currentIndexChanged.connect(
            lambda index: self.ui.report_widget.stacked_reports_forms_layout.setCurrentIndex(index))

        self.ui.report_widget.agreement_gen_widget.generate_button.clicked.connect(self.__gen_agreement_handler)
        self.ui.report_widget.summons_to_court_gen_widget.generate_button.clicked.connect(
            self.__gen_summons_to_court_handler)
        self.ui.report_widget.bank_report_gen_widget.generate_button.clicked.connect(self.__gen_bank_report_handler)

    def __gen_agreement_handler(self):
        item = self.ui.report_widget.agreement_gen_widget.report_table.selectionModel()
        item = item.selectedRows()
        if len(item) != 0:
            item = item[0].row()
            row = self.ui.report_widget.agreement_gen_widget.report_table_model.get_row(item)
            status = MyDbRequests.credit_penalty_check(row[2])
            if status > 0:
                status = "с задержками"
            else:
                status = "своевременно"
            res = {"full_name": row[0],
                   "bank_name": f"\"{row[1]}\"",
                   "credit_id": row[2],
                   "issue_date": row[5],
                   "credit_sum": row[3],
                   "credit_term": row[7],
                   "end_date": row[6],
                   "pay_status": status}
            pattern = self.ui.report_widget.agreement_gen_widget.pattern_text.toPlainText()
            self.ui.report_widget.agreement_gen_widget.messages_text.setText(pattern.format(**res))
        else:
            QMessageBox.warning(self.ui.obj, "Wrong selection", "Select credit before set payment for it")

    def __gen_summons_to_court_handler(self):
        item = self.ui.report_widget.summons_to_court_gen_widget.report_table.selectionModel()
        item = item.selectedRows()
        if len(item) != 0:
            item = item[0].row()
            row = self.ui.report_widget.summons_to_court_gen_widget.report_table_model.get_row(item)
            res = {"full_name": row[0],
                   "bank_name": row[1],
                   "credit_id": row[2],
                   "issue_date": row[3],
                   "end_date": row[4],
                   "credit_sum": row[5],
                   "percentage": row[6],
                   "current_date": MyDbRequests.get_current_date()}
            pattern = self.ui.report_widget.summons_to_court_gen_widget.pattern_text.toPlainText()
            self.ui.report_widget.summons_to_court_gen_widget.messages_text.setText(pattern.format(**res))
        else:
            QMessageBox.warning(self.ui.obj, "Wrong selection", "Select credit before set payment for it")

    def __gen_bank_report_handler(self):
        item = self.ui.report_widget.bank_report_gen_widget.report_table.selectionModel()
        item = item.selectedRows()
        if len(item) != 0:
            item = item[0].row()
            row = self.ui.report_widget.bank_report_gen_widget.report_table_model.get_row(item)
            res = {"bank_name": row[0],
                   "sum": row[1],
                   "payments": row[4],
                   "penalties": row[5],
                   "work_time_period": f"с {row[2]} по {row[3]}"}
            pattern = self.ui.report_widget.bank_report_gen_widget.pattern_text.toPlainText()
            self.ui.report_widget.bank_report_gen_widget.messages_text.setText(pattern.format(**res))
        else:
            QMessageBox.warning(self.ui.obj, "Wrong selection", "Select credit before set payment for it")

    def __data_area_actions(self):
        self.ui.data_visualization_widget.database_combobox.currentIndexChanged.connect(
            lambda index: self.ui.data_visualization_widget.stacked_tables.setCurrentIndex(index))

        self.ui.data_visualization_widget.update_info_button.clicked.connect(
            self.ui.data_visualization_widget.update)

        self.ui.data_visualization_widget.update_info_button.clicked.connect(self.__update_tables_info)

        self.ui.data_visualization_widget.clients_search_table \
            .add_new_instance_button.clicked.connect(self.__adding_new_client_handler)

        self.ui.data_visualization_widget.credit_search_table \
            .add_new_instance_button.clicked.connect(self.__adding_new_credit_handler)

        self.ui.data_visualization_widget.clients_search_table.search_lines["client"] \
            .textEdited.connect(self.__client_table_search)
        self.ui.data_visualization_widget.clients_search_table.search_lines["client_id"] \
            .textEdited.connect(self.__client_table_search)

        for search_line in self.ui.data_visualization_widget.credit_search_table.search_lines.values():
            search_line.textEdited.connect(self.__credit_table_search)

        for search_line in self.ui.data_visualization_widget.protocols_search_table.search_lines.values():
            search_line.textEdited.connect(self.__protocol_table_search)

        self.ui.data_visualization_widget.credit_search_table.make_pay_button.clicked.connect(
            self.__make_pay_for_credit)
        self.ui.data_visualization_widget.credit_search_table.pay_all_owe_button.clicked.connect(
            self.__pay_all_owe_handler)

    def __update_tables_info(self):
        self.ui.data_visualization_widget.credit_search_table.data_table_model.update_info()
        self.ui.data_visualization_widget.clients_search_table.data_table_model.update_info()
        self.ui.data_visualization_widget.protocols_search_table.data_table_model.update_info()
        self.ui.data_visualization_widget.credit_search_table.data_table.update()
        self.ui.data_visualization_widget.clients_search_table.data_table.update()
        self.ui.data_visualization_widget.protocols_search_table.data_table.update()

    def __pay_all_owe_handler(self):
        item = self.ui.data_visualization_widget.credit_search_table.data_table.selectionModel()
        item = item.selectedRows()
        if len(item) != 0:
            item = item[0].row()
            row = self.ui.data_visualization_widget.credit_search_table.data_table_model.get_row(item)
            credit_id = row[3]
            reply = QMessageBox.question(self.ui.obj, "Clarification of intent", "Are you really want to pay all owe?")
            if reply == QMessageBox.StandardButton.Yes:
                MyDbRequests.make_payment(credit_id, MyDbRequests.get_balance_owed(credit_id))
                # MyDbRequests.close_payment(credit_id)
        else:
            QMessageBox.warning(self.ui.obj, "Wrong selection", "Select credit before set payment for it")

    def __make_pay_for_credit(self):
        item = self.ui.data_visualization_widget.credit_search_table.data_table.selectionModel()
        item = item.selectedRows()
        if len(item) != 0:
            item = item[0].row()
            row = self.ui.data_visualization_widget.credit_search_table.data_table_model.get_row(item)
            credit_id = row[3]
            val, okPressed = QInputDialog.getInt(self.ui.obj, "Get payment value",
                                                 f"Payment (awaiting {MyDbRequests.get_month_payment(credit_id)}):",
                                                 0, 0, 10**6, 1)
            if okPressed:
                MyDbRequests.make_payment(credit_id, val)
                self.ui.data_visualization_widget.credit_search_table.data_table_model.update_info()
                self.ui.data_visualization_widget.credit_search_table.update()
                self.__credit_table_search()
        else:
            QMessageBox.warning(self.ui.obj, "Wrong selection", "Select credit before set payment for it")

    def __client_table_search(self, text):
        client_name = self.ui.data_visualization_widget.clients_search_table.search_lines["client"].text()
        client_id = self.ui.data_visualization_widget.clients_search_table.search_lines["client_id"].text()
        self.ui.data_visualization_widget.clients_search_table.data_table_model.update_info(client_name, client_id)
        self.ui.data_visualization_widget.clients_search_table.update()

    def __credit_table_search(self):
        name = self.ui.data_visualization_widget.credit_search_table.search_lines["client"].text()
        bank = self.ui.data_visualization_widget.credit_search_table.search_lines["bank"].text()
        date = self.ui.data_visualization_widget.credit_search_table.search_lines["date"].text()
        credit_id = self.ui.data_visualization_widget.credit_search_table.search_lines["credit_id"].text()
        self.ui.data_visualization_widget.credit_search_table.data_table_model \
            .update_info(credit_id, name, bank, date)

    def __protocol_table_search(self):
        name = self.ui.data_visualization_widget.protocols_search_table.search_lines["client"].text()
        bank = self.ui.data_visualization_widget.protocols_search_table.search_lines["bank"].text()
        date = self.ui.data_visualization_widget.protocols_search_table.search_lines["date"].text()
        credit_id = self.ui.data_visualization_widget.protocols_search_table.search_lines["credit_id"].text()
        self.ui.data_visualization_widget.protocols_search_table.data_table_model \
            .update_info(credit_id, name, bank, date)

    def __adding_new_client_handler(self):
        dialog = AddingNewClientDialog(self.ui.obj)
        dialog.object_setted_up.connect(lambda res: (MyDbRequests.add_new_client(res["name"]),
                                                     self.ui.data_visualization_widget.clients_search_table
                                                     .data_table_model.update_info(),
                                                     self.ui.data_visualization_widget.clients_search_table.update()))
        dialog.show()

    def __adding_new_credit_handler(self):
        dialog = AddingNewCreditDialog(self.ui.obj)
        dialog.object_setted_up.connect(lambda res: (MyDbRequests.add_new_credit(**res),
                                                     self.ui.data_visualization_widget.credit_search_table
                                                     .data_table_model.update_info(),
                                                     self.ui.data_visualization_widget.credit_search_table.update()))
        dialog.show()
        self.__update_tables_info()
