from PyQt5 import QtWidgets
from PyQt5.QtGui import QIntValidator
from PyQt5.QtCore import pyqtSignal

from DB_class import MyDbRequests


class AddingNewCreditDialog(QtWidgets.QDialog):
    object_setted_up = pyqtSignal(dict)

    class MaxValueError(Exception):
        pass

    def __init__(self, parent: QtWidgets.QWidget):
        super(AddingNewCreditDialog, self).__init__(parent)
        self.max_sum = 1000000
        self.setModal(True)

        self.dialog_layout = QtWidgets.QVBoxLayout(self)

        self.button_box = QtWidgets.QDialogButtonBox(self)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.StandardButton.Ok |
                                           QtWidgets.QDialogButtonBox.StandardButton.Cancel)
        self.button_box.rejected.connect(self.close)
        self.button_box.accepted.connect(self.accept_handler)

        self.value = QtWidgets.QLineEdit()
        self.value.setValidator(QIntValidator(0, self.max_sum))

        self.bank_name_line = QtWidgets.QLineEdit()

        self.client_combobox = QtWidgets.QComboBox()
        self.client_combobox.addItems([", ".join(row[0:2]) for row in MyDbRequests.get_all_clients()])
        self.client_combobox.currentIndexChanged.connect(self.__update_dialog_for_client)

        self.months_line = QtWidgets.QLineEdit()
        self.months_line.setValidator(QIntValidator(0, 500))

        form_layout = QtWidgets.QFormLayout()
        form_layout.addRow("Client:", self.client_combobox)
        form_layout.addRow("Bank name:", self.bank_name_line)
        form_layout.addRow("Money value:", self.value)
        form_layout.addRow("Months:", self.months_line)

        self.dialog_layout.addLayout(form_layout)
        self.dialog_layout.addWidget(self.button_box)
        self.__update_dialog_for_client()

    def accept_handler(self):
        result = {}
        try:
            result["money_sum"] = int(self.value.text())
            if result["money_sum"] > self.max_sum:
                raise AddingNewCreditDialog.MaxValueError
            result["bank_name"] = self.bank_name_line.text()
            result["client_id"] = self.client_combobox.currentText().split(',')[0]
            result["months"] = int(self.months_line.text())

            self.object_setted_up.emit(result)
            self.close()
        except AddingNewCreditDialog.MaxValueError:
            QtWidgets.QMessageBox.warning(self, "Wrong money value",
                                          f"You can gen credit not greater then {self.max_sum}")
            self.show()
        except:
            QtWidgets.QMessageBox.warning(self, "Wrong settings", "You don't fill all cells")
        finally:
            self.show()

    def __update_dialog_for_client(self):
        client_id = self.client_combobox.currentText().split(',')[0]
        self.max_sum = int(MyDbRequests.get_max_available_credit_value_for_client(client_id))
        self.value.clear()
        self.value.setValidator(QIntValidator(0, self.max_sum))
