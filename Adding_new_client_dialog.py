from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal

from DB_class import MyDbRequests


class AddingNewClientDialog(QtWidgets.QDialog):
    object_setted_up = pyqtSignal(dict)

    def __init__(self, parent: QtWidgets.QWidget):
        super(AddingNewClientDialog, self).__init__(parent)

        self.setModal(True)
        self.selected_contracts = set()

        self.dialog_layout = QtWidgets.QVBoxLayout(self)

        self.button_box = QtWidgets.QDialogButtonBox(self)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.StandardButton.Ok |
                                           QtWidgets.QDialogButtonBox.StandardButton.Cancel)
        self.button_box.rejected.connect(self.close)
        self.button_box.accepted.connect(self.accept_handler)

        self.name_line = QtWidgets.QLineEdit(self)
        self.second_name_line = QtWidgets.QLineEdit(self)
        self.third_name_line = QtWidgets.QLineEdit(self)

        form_layout = QtWidgets.QFormLayout()
        form_layout.addRow("Client name:", self.name_line)
        form_layout.addRow("Second name:", self.second_name_line)
        form_layout.addRow("Third name:", self.third_name_line)

        self.dialog_layout.addLayout(form_layout)
        self.dialog_layout.addWidget(self.button_box)

    def accept_handler(self):
        result = {}
        try:
            name = self.name_line.text().strip().title()
            second_name = self.second_name_line.text().strip().title()
            third_name = self.third_name_line.text().strip().title()

            if len(name) < 2 or len(second_name) < 2 or len(third_name) < 2:
                raise ValueError("Name length err")
            else:
                result["name"] = name + ' ' + second_name + ' ' + third_name

            self.object_setted_up.emit(result)
            self.close()
        except:
            QtWidgets.QMessageBox.warning(self, "Wrong settings", "You don't fill all cells")
            self.show()
