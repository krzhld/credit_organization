from typing import Optional
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtGui import QIntValidator

from Uni_table_model import UnifiedTableModel
from Table_data_area_widget import TableDataArea, SearchTableArea
from DB_class import MyDbRequests
from Messages_on_data_wiget import WarningMessageGenerationWidget


class MainWindowUI:

    # Widgets with handling actions should be a *UI class field if it isn't don't use class object field.

    def __init__(self, obj: QtWidgets.QMainWindow or QtWidgets.QWidget):
        self.obj = obj
        self.main_widget = QtWidgets.QWidget(self.obj)
        self.obj.setCentralWidget(self.main_widget)
        self.__init_main_window_ui()
        self.__init_main_widget_ui()

    def __init_main_window_ui(self):
        self.obj.setWindowTitle("Credit system")
        self.obj.setMinimumSize(800, 600)

        # Moving window on center of screen
        screen_size = QtWidgets.QApplication.primaryScreen().geometry().size()
        x = (screen_size.width() - self.obj.width()) // 2
        y = (screen_size.height() - self.obj.height()) // 2
        self.obj.move(x, y)

    def __init_main_widget_ui(self):
        self.main_layout = QtWidgets.QVBoxLayout(self.main_widget)
        self.main_widget.setLayout(self.main_layout)
        self.__center_tab_widget()

    def __center_tab_widget(self):
        date_layout = QtWidgets.QHBoxLayout()
        self.date_line = QtWidgets.QLineEdit(MyDbRequests.get_current_date())
        self.date_line.setDisabled(True)
        self.date_line.setSizePolicy(
            QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum))
        self.date_line.setAlignment(QtCore.Qt.AlignCenter)
        spacer = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding)
        self.time_flow_button = QtWidgets.QRadioButton("Stop time flow")

        date_layout.addWidget(QtWidgets.QLabel("Virtual date:"))
        date_layout.addWidget(self.date_line)
        date_layout.addSpacerItem(spacer)
        date_layout.addWidget(self.time_flow_button)

        self.center_tab_widget = QtWidgets.QTabWidget(self.main_widget)

        self.main_layout.addLayout(date_layout)
        self.main_layout.addWidget(self.center_tab_widget)
        self.center_tab_widget.tabBar().setExpanding(True)

        self.report_widget = self.ReportArea(self.main_widget)
        self.data_visualization_widget = self.DataVisualizationArea(self.main_widget)

        self.center_tab_widget.addTab(self.report_widget, "Reports")
        self.center_tab_widget.addTab(self.data_visualization_widget, "Data")

    class ReportArea(QtWidgets.QWidget):
        class ReportTableViewWidget(QtWidgets.QWidget):
            INSTANCES = []

            def __init__(self, parent, table_titles, update_info_function):
                super().__init__(parent)

                self.INSTANCES.append(self)

                layout = QtWidgets.QVBoxLayout(self)

                self.report_table_model = UnifiedTableModel(table_titles, update_info_function)
                self.report_table = QtWidgets.QTableView(self)
                self.report_table.setModel(self.report_table_model)
                self.report_table.horizontalHeader() \
                    .setSectionResizeMode(QtWidgets.QHeaderView.ResizeMode.ResizeToContents)

                layout.addWidget(self.report_table)

            def update(self) -> None:
                self.report_table_model.update_info()
                self.report_table.update()
                super().update()

        def __init__(self, parent: Optional[QtWidgets.QWidget]):
            super().__init__(parent)
            self.setParent(parent)

            self.autoupdate_flag = True

            vertical_layout = QtWidgets.QVBoxLayout(self)

            self.report_combobox = QtWidgets.QComboBox(self)
            self.report_combobox.addItems(["Summons to court",
                                           "Agreement",
                                           "Bank reports"])

            with open("subpoena_sample.txt", 'r') as file:
                summons_to_court_message_pattern = file.read()
            with open("absent_responsobility_sample.txt", 'r') as file:
                agreement_message_pattern = file.read()
            with open("financial_report_sample.txt", 'r') as file:
                bank_report_pattern = file.read()

            self.summons_to_court_gen_widget = WarningMessageGenerationWidget(self,
                                                                              ["Client",
                                                                               "Bank",
                                                                               "Credit ID",
                                                                               "Issue date",
                                                                               "End date",
                                                                               "Credit",
                                                                               "Percent",
                                                                               "Penalty"],
                                                                              MyDbRequests.get_summons_to_court,
                                                                              summons_to_court_message_pattern)

            self.agreement_gen_widget = WarningMessageGenerationWidget(self,
                                                                       ["Client",
                                                                        "Bank",
                                                                        "Credit ID",
                                                                        "Credit",
                                                                        "Percent",
                                                                        "Issue date",
                                                                        "End date",
                                                                        "Credit term",
                                                                        "Type credit story"],
                                                                       MyDbRequests.get_list_for_agreements,
                                                                       agreement_message_pattern)

            self.bank_report_gen_widget = WarningMessageGenerationWidget(self,
                                                                         ["Bank",
                                                                          "Given",
                                                                          "Period from",
                                                                          "Period to",
                                                                          "Received",
                                                                          "Penalties"],
                                                                         MyDbRequests.get_bank_info,
                                                                         bank_report_pattern)

            self.stacked_reports_forms_layout = QtWidgets.QStackedLayout()
            self.stacked_reports_forms_layout.addWidget(self.summons_to_court_gen_widget)
            self.stacked_reports_forms_layout.addWidget(self.agreement_gen_widget)
            self.stacked_reports_forms_layout.addWidget(self.bank_report_gen_widget)

            form_layout = QtWidgets.QFormLayout()
            form_layout.addRow("Report type:", self.report_combobox)

            vertical_layout.addLayout(form_layout)
            vertical_layout.addLayout(self.stacked_reports_forms_layout)

        def update(self) -> None:
            for instance in MainWindowUI.ReportArea.ReportTableViewWidget.INSTANCES:
                instance.update()
            super().update()

        def __update_report_text(self, text):
            if self.autoupdate_flag:
                vsb = self.payslip_report_text.verticalScrollBar()
                old_pos_ratio = vsb.value() / (vsb.maximum() or 1)
                self.payslip_report_text.setText(text)
                self.payslip_report_text.verticalScrollBar().setValue(int(old_pos_ratio * vsb.maximum()))

    class DataVisualizationArea(QtWidgets.QWidget):
        def __init__(self, parent: Optional[QtWidgets.QWidget]):
            super().__init__()
            self.setParent(parent)

            vertical_layout = QtWidgets.QVBoxLayout(self)
            form_layout = QtWidgets.QFormLayout()
            self.stacked_tables = QtWidgets.QStackedLayout()

            self.database_combobox = QtWidgets.QComboBox(self)
            self.database_combobox.addItems(["Clients", "Credits", "Protocols"])

            self.update_info_button = QtWidgets.QPushButton("Update info")

            form_layout.addRow("Data", self.database_combobox)

            self.clients_search_table = SearchTableArea(self, "client",
                                                        ["Client id", "Full name", "Reputation"],
                                                        MyDbRequests.get_all_clients,
                                                        [["client", "Client name:"],
                                                         ["client_id", "Client id:"]])

            self.credit_search_table = SearchTableArea(self, "credit",
                                                       ["Client id", "Full name",
                                                        "Bank", "Credit ID",
                                                        "Credit", "Early repayment",
                                                        "Percentage", "Issue date",
                                                        "End date", "Status"],
                                                       MyDbRequests.get_all_credits_history,
                                                       [["credit_id", "Credit ID:"],
                                                        ["client", "Client name:"],
                                                        ["bank", "Bank name"],
                                                        ["date", "Date"]])

            self.protocols_search_table = SearchTableArea(self, "protocol",
                                                          ["Client id", "Full name",
                                                           "Bank", "Credit ID",
                                                           "From date", "To date",
                                                           "Payment plan", "Real payment",
                                                           "Penalty"],
                                                          MyDbRequests.get_all_protocols_history,
                                                          [["credit_id", "Credit ID:"],
                                                           ["client", "Client name:"],
                                                           ["bank", "Bank name"],
                                                           ["date", "Date"]])

            self.credit_search_table.search_lines["credit_id"].setValidator(QIntValidator(0, 100000000))
            self.protocols_search_table.search_lines["credit_id"].setValidator(QIntValidator(0, 100000000))
            self.protocols_search_table.layout.removeWidget(self.protocols_search_table.add_new_instance_button)
            del self.protocols_search_table.add_new_instance_button
            self.credit_search_table.make_pay_button = QtWidgets.QPushButton("Make pay")
            self.credit_search_table.pay_all_owe_button = QtWidgets.QPushButton("Pay all owe")
            self.credit_search_table.layout.addWidget(self.credit_search_table.make_pay_button)
            self.credit_search_table.layout.addWidget(self.credit_search_table.pay_all_owe_button)

            self.stacked_tables.addWidget(self.clients_search_table)
            self.stacked_tables.addWidget(self.credit_search_table)
            self.stacked_tables.addWidget(self.protocols_search_table)

            vertical_layout.addLayout(form_layout)
            vertical_layout.addWidget(self.update_info_button)
            vertical_layout.addLayout(self.stacked_tables)
