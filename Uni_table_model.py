from typing import Any

from PyQt5.QtCore import Qt, QAbstractTableModel


class UnifiedTableModel(QAbstractTableModel):
    def __init__(self, table_titles, update_function):
        super().__init__()
        self._data = None
        self._update_function = update_function
        self._table_titles = table_titles
        self.update_info()

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...) -> Any:
        if role == Qt.ItemDataRole.DisplayRole and orientation == Qt.Orientation.Horizontal:
            return self._table_titles[section]

    def data(self, index, role):
        if role == Qt.ItemDataRole.DisplayRole or role == Qt.ItemDataRole.EditRole:
            return self._data[index.row()][index.column()]
        if role == Qt.ItemDataRole.TextAlignmentRole:
            return Qt.AlignmentFlag.AlignCenter

    def rowCount(self, index):
        return len(self._data)

    def columnCount(self, index):
        return len(self._table_titles)

    def update_info(self, *args, **kwargs):
        self.beginResetModel()
        self._data = self._update_function(*args, **kwargs)
        self.endResetModel()

    def get_row(self, row):
        return self._data[row]

    def search_index_in_column(self, column_name, value, count=0):
        try:
            column_index = self._table_titles.index(column_name)

            value = type(self._data[0][column_index])(value)

            for i, item in enumerate(self._data):
                if item[column_index] == value:
                    if count == 0:
                        return i
                    count -= 1
            return None
        except:
            return None
