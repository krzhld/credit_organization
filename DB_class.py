from sqlalchemy import sql, create_engine


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonMeta, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class MyDbRequests(metaclass=SingletonMeta):
    SERVER = create_engine("postgresql://user_name:pass@localhost/postgres",
                           echo=False, pool_size=30,
                           max_overflow=20,
                           pool_timeout=10,
                           future=True,
                           isolation_level="AUTOCOMMIT")

    @staticmethod
    def evaluate_query(query):
        with MyDbRequests.SERVER.begin() as connection:
            query = sql.text(query)
            result = connection.execute(query)
            return [[str(val) for val in row] for row in result]

    @staticmethod
    def evaluate_query_without_return(query):
        with MyDbRequests.SERVER.begin() as connection:
            query = sql.text(query)
            connection.execute(query)

    @staticmethod
    def __separate_table(input_data):
        return [list(row) for row in input_data]

    @staticmethod
    def add_new_credit(client_id, bank_name, money_sum, months):
        res = MyDbRequests.evaluate_query(
            f"""
            INSERT INTO credits(client_id, bank_name, credit_amount, issue_date, month_to_pay)
            VALUES
            (	
                {client_id},
                \'{bank_name}\',
                {money_sum},
                (SELECT date FROM global_date),
                {months}
            )
            RETURNING credit_id
            """
        )
        MyDbRequests.evaluate_query_without_return(
            f"""
            INSERT INTO protocols (credit_id, plan_payment_date, plan_payment_amount)
            SELECT credit_id, MONTH, plan_payment_amount FROM protocols_v
            WHERE month >= (SELECT date FROM global_date)
            AND MONTH < (SELECT date + INTERVAL'1month' FROM global_date)
            AND credit_id = {res[0][0]}
            """
        )
        MyDbRequests.evaluate_query_without_return(
            """   
            WITH duplicate_mark AS (
                SELECT *,
                ROW_NUMBER() OVER (PARTITION BY credit_id,
                 plan_payment_date order BY (credit_id, plan_payment_date)) AS mark
                FROM protocols p 
            )
            DELETE FROM protocols
            WHERE protocol_id IN (SELECT protocol_id FROM duplicate_mark WHERE mark >= 2)
            """
        )

    @staticmethod
    def get_all_credits_history(credit_id=None, name_pattern=None, bank_pattern=None, date_pattern=None):
        pattern = ''
        if credit_id is not None:
            if len(credit_id) > 0:
                pattern += f"credit_id = {credit_id}\n"
        if name_pattern is not None:
            name_pattern = name_pattern.strip().split()
            for name in name_pattern:
                if pattern != '':
                    pattern += "AND "
                pattern += f"full_name LIKE \'%{name}%\'\n"
        if bank_pattern is not None:
            bank_pattern = bank_pattern.strip().split(';')
            if pattern != '':
                pattern += "AND ("
            else:
                pattern += "("

            val = [f"bank_name LIKE \'%{name}%\'\n" for name in bank_pattern]
            val = " OR ".join(val)
            pattern += val + ')\n'
        if date_pattern is not None:
            if pattern != '':
                pattern += "AND "
            pattern += f"issue_date::text LIKE \'%{date_pattern}%\'\n"

        if pattern != '':
            pattern = "WHERE\n" + pattern
            pattern += "AND status LIKE \'IN PROCESS\'\n"
        else:
            pattern = "WHERE status LIKE \'IN PROCESS\'"
        return MyDbRequests.evaluate_query(
            f"""
            SELECT 
                cl.client_id,
                full_name,
                bank_name,
                credit_id,
                credit_amount::text || '$',
                early_repayment::text || '$',
                annual_percentage::text || '%',
                issue_date,
                (issue_date + INTERVAL '1month' * month_to_pay)::date AS end_date,
                status
                FROM credits cr
            JOIN clients cl ON cl.client_id = cr.client_id
            {pattern}
            ORDER BY full_name, credit_amount, issue_date
            """
        )

    @staticmethod
    def make_payment(credit_id, payment):
        MyDbRequests.evaluate_query_without_return(
            f"""
            UPDATE protocols SET 
                penalty = penalty - {payment}
            WHERE credit_id = {credit_id}
            AND (SELECT date FROM global_date) >= plan_payment_date - INTERVAL'1month'
            AND (SELECT date FROM global_date) < plan_payment_date
            RETURNING *
            """
        )
        MyDbRequests.evaluate_query_without_return(
            """
            WITH real_payment_amount_update AS(
                UPDATE protocols SET
                    real_payment_amount = real_payment_amount - penalty,
                    penalty = 0
                WHERE penalty < 0
                RETURNING *
            )
            UPDATE credits c SET
                early_repayment = early_repayment + 
                COALESCE ((SELECT real_payment_amount - plan_payment_amount 
                FROM real_payment_amount_update pu
                WHERE pu.credit_id = c.credit_id), 0)
            WHERE credit_id IN (SELECT credit_id FROM real_payment_amount_update u)
            RETURNING *
            """
        )
        MyDbRequests.evaluate_query_without_return(
            """
            WITH client AS (
                (SELECT DISTINCT ON (credit_id) client_id
                FROM protocols_v pv 
                WHERE plan_payment_amount <= 0
                AND credit_id NOT IN 
                    (SELECT credit_id FROM protocols where penalty > 0))
            )
            UPDATE clients c SET
                type_credit_story = 'good'
            WHERE client_id = (SELECT * FROM client)
                AND (SELECT type_credit_story FROM clients WHERE client_id = (SELECT * FROM client)) NOT LIKE 'bad'
            """
        )
        MyDbRequests.evaluate_query_without_return(
            """
            WITH client AS (
                (SELECT DISTINCT ON (credit_id) client_id
                FROM protocols_v pv 
                WHERE plan_payment_amount <= 0
                AND credit_id NOT IN 
                    (SELECT credit_id FROM protocols where penalty < 0))
            )
            UPDATE clients c SET
                type_credit_story = 'bad'
            WHERE client_id = (SELECT * FROM client)
            """
        )
        MyDbRequests.evaluate_query_without_return(
            """   
            --finish credit
            WITH last_penalty AS (
                SELECT credit_id, penalty
                FROM protocols p2 
                WHERE (SELECT date FROM global_date) >= plan_payment_date - INTERVAL '1 month'
                AND (SELECT date FROM global_date) < plan_payment_date
            ), payment_statistic AS(
                SELECT
                    p.credit_id,
                    sum(plan_payment_amount) AS sum_plan_payment_amount,
                    sum(real_payment_amount) AS sum_real_payment_amount,
                    COALESCE(sum(lp.penalty), 0) AS last_penalty,
                    sum(real_payment_amount) - sum(plan_payment_amount) - COALESCE(sum(lp.penalty), 0) AS payment_result
                FROM full_time_protocols p 
                LEFT OUTER JOIN last_penalty lp ON lp.credit_id = p.credit_id
                GROUP BY p.credit_id
            ), should_to_close_credits_id AS(
                SELECT c.credit_id 
                FROM payment_statistic ps
                JOIN credits c ON c.credit_id = ps.credit_id AND status NOT LIKE 'PAID'
                WHERE payment_result >= 0
            )
            UPDATE credits SET
                status = 'PAID'
            WHERE credit_id IN (SELECT credit_id FROM should_to_close_credits_id)
            """
        )

    @staticmethod
    def close_payment(credit_id):
        MyDbRequests.evaluate_query_without_return(
            f"""   
            --finish credit
            UPDATE credits c SET
                status = 'PAID',
                month_to_pay = CASE
                                WHEN (month_diff(
                                issue_date,
                                (SELECT month FROM protocols_v pv 
                                WHERE credit_id = pv.credit_id
                                ORDER BY MONTH
                                LIMIT 1)) NOTNULL) 
                                THEN 
                                month_diff(
                                issue_date,
                                (SELECT month FROM protocols_v pv 
                                WHERE credit_id = pv.credit_id
                                ORDER BY MONTH
                                LIMIT 1))
                                ELSE month_to_pay
                                END
            WHERE credit_id = {credit_id}
            """
        )
        # MyDbRequests.evaluate_query_without_return(
        #     """
        #     WITH duplicate_mark AS (
        #         SELECT *,
        #         ROW_NUMBER() OVER (PARTITION BY credit_id,
        #          plan_payment_date order BY (credit_id, plan_payment_date)) AS mark
        #         FROM protocols p
        #     )
        #     DELETE FROM protocols
        #     WHERE protocol_id IN (SELECT protocol_id FROM duplicate_mark WHERE mark >= 2)
        #     """
        # )

    @staticmethod
    def get_all_clients(name_pattern='', client_id_pattern=''):
        pattern = ''
        if name_pattern is not None:
            name_pattern = name_pattern.strip().split()
            for name in name_pattern:
                if pattern != '':
                    pattern += "AND "
                pattern += f"full_name LIKE \'%{name}%\'\n"

        if client_id_pattern is not None:
            client_id_pattern = client_id_pattern.strip().split()
            first_insertion_flag = True
            for id_pattern in client_id_pattern:
                if pattern != '':
                    if first_insertion_flag:
                        first_insertion_flag = False
                        pattern += "AND ("
                    else:
                        pattern += "OR "
                else:
                    first_insertion_flag = False
                    pattern += "("
                pattern += f"client_id = {id_pattern}\n"
            if "client_id" in pattern:
                pattern += ')'

            if pattern != '':
                pattern = "WHERE\n" + pattern

        return MyDbRequests.evaluate_query(
            f"""
            SELECT client_id, full_name, type_credit_story FROM clients c 
            {pattern}
            ORDER BY full_name
            """
        )

    @staticmethod
    def add_new_client(name):
        MyDbRequests.evaluate_query_without_return(
            f"""
            INSERT INTO clients(full_name) VALUES (\'{name}\')
            """
        )

    @staticmethod
    def client_credit_story_type(client_id):
        return MyDbRequests.evaluate_query(
            f"""
            SELECT DISTINCT ON (type_credit_story) type_credit_story FROM clients cl
            JOIN credits cr ON cr.client_id = cl.client_id 
            WHERE cl.client_id = {client_id}
            """
        )

    @staticmethod
    def get_max_available_credit_value_for_client(client_id):
        return float(
            MyDbRequests.evaluate_query(
                f"""
                WITH credit_story_type AS (
                    SELECT DISTINCT ON (cl.client_id)
                        cl.client_id,
                        COALESCE(type_credit_story, 'clear') AS type_credit_story
                    FROM clients cl
                    LEFT OUTER JOIN credits cr ON cr.client_id = cl.client_id
                    WHERE cl.client_id = {client_id}
                )
                , client_max_credit AS (
                    SELECT DISTINCT ON (type_credit_story)
                        cs.client_id,
                        type_credit_story,
                        COALESCE(max(credit_amount),0) AS max,
                        COALESCE(min(credit_amount),0) AS min
                    FROM credits c
                    RIGHT OUTER JOIN credit_story_type cs ON cs.client_id = c.client_id
                    GROUP BY cs.type_credit_story, cs.client_id
                )
                SELECT
                    CASE 
                        WHEN (type_credit_story LIKE 'clear') THEN 1e6
                        WHEN (type_credit_story LIKE 'good' AND max * 1.15 < 1e6) THEN 1e6
                        WHEN (type_credit_story LIKE 'good' AND max * 1.15 >= 1e6) THEN max * 1.15
                        ELSE min * 0.75
                    END AS max_available_credit
                FROM client_max_credit
                """
            )[0][0]
        )

    @staticmethod
    def get_month_payment(credit_id):
        try:
            return float(
                MyDbRequests.evaluate_query(
                    f"""
                    SELECT 
                        CASE 
                            WHEN (plan_payment_amount - real_payment_amount >= 0) THEN plan_payment_amount - real_payment_amount
                            ELSE 0
                        END + penalty AS need_to_pay
                    FROM protocols p2 
                    WHERE (SELECT date FROM global_date) >= plan_payment_date - INTERVAL '1 month'
                    AND (SELECT date FROM global_date) < plan_payment_date
                    AND credit_id = {credit_id}
                    """
                )[0][0]
            )
        except:
            return 0

    @staticmethod
    def get_current_date():
        return MyDbRequests.evaluate_query(
            """
                SELECT date from global_date
            """
        )[0][0]

    @staticmethod
    def next_day():
        MyDbRequests.evaluate_query_without_return(
            """
            /*Date update circle*/
            UPDATE protocols SET 
                plan_payment_amount = plan_payment_amount + penalty * 0.01
            WHERE (SELECT date FROM global_date) >= plan_payment_date - INTERVAL'1month'
                AND (SELECT date FROM global_date) < plan_payment_date;

            UPDATE global_date SET
                date = date + 1
            WHERE TRUE;

            /*Month update*/
            WITH bad_pay_credits AS (
                SELECT * FROM credits c 
                WHERE (date_trunc('month', issue_date) + (month_to_pay)*INTERVAL'1month')
                <= (SELECT date FROM global_date gd)
                AND status NOT LIKE 'PAID'
            ), clients_marking AS (
                UPDATE clients SET
                    type_credit_story = 'bad'
                WHERE client_id IN (SELECT client_id FROM bad_pay_credits)
            ) UPDATE credits SET
                month_to_pay = month_to_pay + 1
            WHERE credit_id IN (SELECT credit_id FROM bad_pay_credits);
            
            INSERT INTO protocols (credit_id, plan_payment_date, plan_payment_amount)
            SELECT credit_id, MONTH, plan_payment_amount FROM protocols_v
            WHERE (SELECT date FROM global_date) >= MONTH - INTERVAL'1month'
                AND (SELECT date FROM global_date) < month
                AND date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
                = pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date))
                AND (credit_id, MONTH) NOT IN (SELECT credit_id, plan_payment_date FROM protocols);
                
            WITH user_payment_update as(
                SELECT
                    *
                FROM protocols p
                WHERE (SELECT date FROM global_date) >= p.plan_payment_date
                AND (SELECT (date - INTERVAL '1month')::date FROM global_date) < p.plan_payment_date
                AND date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
                = pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date))
            )
            UPDATE protocols ps SET
                penalty = COALESCE((SELECT 
                DISTINCT ON (u.protocol_id)
                    u.penalty +
                    COALESCE(
                    CASE
                        WHEN (u.real_payment_amount < u.plan_payment_amount)
                            THEN u.plan_payment_amount - u.real_payment_amount
                        ELSE 0
                    END,
                    0)
                    FROM user_payment_update u
                    WHERE date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
                        = pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date))
                    AND u.credit_id = ps.credit_id
                    AND (u.plan_payment_date + INTERVAL '1 month')::date = ps.plan_payment_date), 0)
            WHERE 
                (ps.credit_id, (ps.plan_payment_date - INTERVAL '1 month')::date)
                    IN (SELECT credit_id, plan_payment_date FROM user_payment_update)
                AND 
                date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
                = pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date));
                
            UPDATE global_date SET
            check_new_month = date_trunc('month', date) 
            WHERE 
                date_trunc('month', (SELECT date FROM global_date) - INTERVAL'1month')
                = pg_catalog.date_trunc('month',(SELECT check_new_month FROM global_date))
            """
        )

    @staticmethod
    def get_balance_owed(credit_id):
        return float(
            MyDbRequests.evaluate_query(
                f"""
                WITH last_penalty AS (
                    SELECT credit_id, penalty
                    FROM protocols p2 
                    WHERE (SELECT date FROM global_date) >= plan_payment_date - INTERVAL '1 month'
                    AND (SELECT date FROM global_date) < plan_payment_date
                ), payment_statistic AS(
                    SELECT
                        p.credit_id,
                        sum(plan_payment_amount) AS sum_plan_payment_amount,
                        sum(real_payment_amount) AS sum_real_payment_amount,
                        COALESCE(sum(lp.penalty), 0) AS last_penalty,
                        sum(real_payment_amount) - sum(plan_payment_amount) - COALESCE(sum(lp.penalty), 0) AS payment_result
                    FROM full_time_protocols p 
                    LEFT OUTER JOIN last_penalty lp ON lp.credit_id = p.credit_id
                    GROUP BY p.credit_id
                )SELECT -payment_result FROM payment_statistic
                WHERE payment_result < 0 AND credit_id = {credit_id}
                """
            )[0][0]
        )

    @staticmethod
    def get_summons_to_court():
        return MyDbRequests.evaluate_query(
            """
            SELECT 
                full_name,
                bank_name,
                c.credit_id,
                issue_date,
                issue_date + month_to_pay,
                credit_amount::text || '$',
                annual_percentage::text || '%',
                penalty::text || '$'
            FROM protocols p
            JOIN credits c ON 
                c.credit_id = p.credit_id
                AND
                p.penalty >= c.credit_amount * 0.1
                AND
                c.status not LIKE 'PAID'
            JOIN clients c2 ON c2.client_id = c.client_id
            WHERE plan_payment_date > (SELECT date FROM global_date gd)
            ORDER BY plan_payment_date
            """
        )

    @staticmethod
    def get_list_for_agreements():
        return MyDbRequests.evaluate_query(
            """
            SELECT 
                full_name,
                bank_name,
                credit_id,
                credit_amount::TEXT || '$' AS credit,
                annual_percentage::TEXT || '%' AS percentage,
                issue_date,
                (issue_date + (INTERVAL '1 month' * month_to_pay))::date AS end_date,
                CASE
                    WHEN (CEIL(month_to_pay / 12) > 0) THEN CEIL(month_to_pay / 12)::TEXT || ' years  ' 
                    || MOD(month_to_pay, 12)::TEXT || ' month'
                    ELSE MOD(month_to_pay, 12)::TEXT || ' month'
                END AS term,
                type_credit_story
            FROM credits c 
            JOIN clients c2 ON c2.client_id = c.client_id
            WHERE status LIKE 'PAID'
            """
        )

    @staticmethod
    def credit_penalty_check(credit_id):
        return float(
            MyDbRequests.evaluate_query(
                f"""
                SELECT max(penalty) FROM protocols p 
                WHERE credit_id = {credit_id}
                """
            )[0][0]
        )

    @staticmethod
    def get_bank_info():
        return MyDbRequests.evaluate_query(
            """
            WITH payment_info AS (
                SELECT
                    bank_name,
                    sum(real_payment_amount) AS get,
                    sum(penalty) AS penny
                FROM protocols p 
                    JOIN credits c ON c.credit_id = p.credit_id
                GROUP BY bank_name
            ) 
            SELECT
                c.bank_name,
                sum(credit_amount) AS given,
                min(issue_date) AS from,
                max(issue_date + month_to_pay) AS TO,
                p.GET,
                penny
            FROM credits c
                JOIN payment_info p ON p.bank_name = c.bank_name
            GROUP BY c.bank_name, p.GET, penny
            ORDER BY c.bank_name
            """
        )

    @staticmethod
    def get_all_protocols_history(credit_id=None, name_pattern=None, bank_pattern=None, date_pattern=None):
        pattern = ''
        if credit_id is not None:
            if len(credit_id) > 0:
                pattern += f"c.credit_id = {credit_id}\n"
        if name_pattern is not None:
            name_pattern = name_pattern.strip().split()
            for name in name_pattern:
                if pattern != '':
                    pattern += "AND "
                pattern += f"full_name LIKE \'%{name}%\'\n"
        if bank_pattern is not None:
            bank_pattern = bank_pattern.strip().split(';')
            if pattern != '':
                pattern += "AND ("
            else:
                pattern += "("

            val = [f"bank_name LIKE \'%{name}%\'\n" for name in bank_pattern]
            val = " OR ".join(val)
            pattern += val + ')\n'
        if date_pattern is not None:
            if pattern != '':
                pattern += "AND "
            pattern += f"issue_date::text LIKE \'%{date_pattern}%\'\n"

        if pattern != '':
            pattern = "WHERE\n" + pattern
            pattern += "AND status LIKE \'IN PROCESS\'\n"
        else:
            pattern = "WHERE status LIKE \'IN PROCESS\'"
        return MyDbRequests.evaluate_query(
            f"""
            SELECT
                c.client_id,
                full_name,
                bank_name,
                c.credit_id,
                (plan_payment_date - INTERVAL'1month')::date::TEXT AS from,
                plan_payment_date::TEXT AS to,
                plan_payment_amount::TEXT || '$' AS plan_payment,
                real_payment_amount::TEXT || '$' AS real_payment,
                penalty::TEXT || '$' AS penalty
            FROM full_time_protocols fp
            JOIN credits c ON fp.credit_id = c.credit_id
            JOIN clients cl ON cl.client_id = c.client_id
            {pattern}
            ORDER BY full_name, bank_name, credit_id, plan_payment_date
            """
        )
